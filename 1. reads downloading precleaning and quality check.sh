#CREATE A NEW FOLDER
/media/nmauri/SHAULA/...

#AVAILABLE SPACE
df -h /media/nmauri/SHAULA/
Filesystem      Size  Used Avail Use% Mounted on
/dev/sda2       3,7T  3,1T  637G  83% /media/nmauri/SHAULA

2,35 G per file R1, R2: 2,35*2*18= 82,8 G

# USERNAME, PASSWORD AND LINK
wget -r -nH --cut-dirs=5 -nc --reject .html --reject .gif --user $USERNAME --password $PASSWORD $HTTP

# VERIFY DATA INTEGRITY 
cd $FOLDER/...
md5sum -c md5sum.txt

# FastQC CHECKPOINT (fastqc <file.fastq.gz>)
gunzip FastQC.tar.gz
tar -xvf FastQC.tar

R1 tienen menor contenido en G mientras que R2, en C

# SAMPLES
ls *R1_001.fastq.gz | sed 's/_R1_001.fastq.gz//g' > samples.list

# TRIMGALORE
# Check that cutadapt is installed
cutadapt --version #3.5
# Check that FastQC is installed
fastqc -v #FastQC v0.11.9
# Install Trim Galore
curl -fsSL https://github.com/FelixKrueger/TrimGalore/archive/0.6.6.tar.gz -o trim_galore.tar.gz
tar xvzf trim_galore.tar.gz
# Run Trim Galore
for i in $(cat samples.list); do TrimGalore-0.6.6/trim_galore --paired ${i}_R1_001.fastq.gz ${i}_R2_001.fastq.gz; done

#>>> Now performing quality (cutoff '-q 20') and adapter trimming in a single pass for the adapter sequence: #'AGATCGGAAGAGC' from file ---_R1_001.fastq.gz <<<

# FastQC checkpoint.
for i in $(ls *val_1.fq.gz); do fastqc ${i}; done
