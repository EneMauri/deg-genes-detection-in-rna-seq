######### EXONIC REGIONS COUNTING ###################
for i in $(cat samples.list)
do 
htseq-count -a30 -t exon -i ID -f bam --stranded=reverse ${i}.markdup.bam PN40024.v4.1.REF.gff3 > ${i}.markdup.exon.counts
done

for i in $(ls *markdup.bam); do samtools view -q30 -c ${i}; done
for i in $(ls *counts); do awk '/^Vit/{sum+=$2} END {print sum}' ${i}

######## TABLE EDITING #############################
ls *.counts |awk -F'_' 'ORS="\t"{print $1"_"$2}' | awk '{print "gene_ID\t"$0}' > header
paste -d'\t' *.counts | cut -f1 > rownames
paste -d'\t' *.counts | awk '{for (i=1;i<=NF;i+=2) $i=""}1' | paste -d' ' rownames - | sed 's/  /\t/g' | cat header <(grep '^Vit' -) > counts_matrix.csv
rm *counts rownames header

# THERE IS NO DIFFERENCE IN COUNTS RESULTS USING MARKDUP OR PREVIOUS TO MARK DUPLICATES, SORTED.BAM.

for i in ABA_M1_11307AAD_AGGACCTA-ATGTATCC.sorted.exon.counts ABA_M1_11307AAD_AGGACCTA-ATGTATCC.markdup.exon.counts; do awk '{sum+=$2} END {print sum}' ${i}; done
#159527287

# HT-COUNT IS COUNTING EACH PAIR AS ONE.
samtools view -c ABA_M1_11307AAD_AGGACCTA-ATGTATCC.markdup.bam
#165167170

#For paired-end data, htseq-count counts read pairs as “units of evidence” for gene expression. 

#samtools view ...markdup.bam| head
#VH00658:8:AAATFM2HV:1:2501:22889:54840	163	chr01	5205	60	50M	=	5256	99	#ACACGCAAGACTCATCATGACACTGATCAAAACCAAAAGTCTTGATCGTC	CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC	MD:Z:50	#PG:Z:MarkDuplicates	XG:i:0	NH:i:1	NM:i:0	XM:i:0	XN:i:0	XO:i:0	AS:i:0	YS:i:0	ZS:i:0	YT:Z:CP
#VH00658:8:AAATFM2HV:1:2501:22889:54840	83	chr01	5256	60	48M	=	5205	-99	#GGTCAAAACAAGTATTCCAAGATCTAGATGCCTGTTTAAGGCCATAAA	CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC;CCCCCCCCCCCCC	MD:Z:48	#PG:Z:MarkDuplicates	XG:i:0	NH:i:1	NM:i:0	XM:i:0	XN:i:0	XO:i:0	AS:i:0	YS:i:0	ZS:i:0	YT:Z:CP

#The two paires are adjadcent one to the other. 

#SORTED AND MARKDUP RESULTS COMPARISON
for i in ${i}.sorted.exon.counts ${i}.markdup.exon.counts; do tail -n5 ${i}; done
#__no_feature	149314871 ## PAIRED?
#__ambiguous	231379
#__too_low_aQual	4518308
#__not_aligned	4918725
#__alignment_not_unique	0

#__no_feature	149314871
#__ambiguous	231379
#__too_low_aQual	4518308
#__not_aligned	4918725
#__alignment_not_unique	0

#NON-STRANDED, STRANDED-DIR, STRANDED-REV 
for i in ${i}.markdup.exon.counts ${i}.markdup.no.exon.counts ${i}.markdup.reverse.exon.counts
do awk '/^Vit/{sum+=$2} END {print sum}' ${i}; done
#544004 # 
#82972064 # --stranded=no
#82576042 # --stranded=reverse *****
