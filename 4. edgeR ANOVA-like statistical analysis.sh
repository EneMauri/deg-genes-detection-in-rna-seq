# Pair-end dataset strand-spec.
# 18 samples and replicates.
# 4 ANOVA-like comparisons.
#Pairwise comparisons:
#a) ABA_MAT vs C_MAT
#b) POL_MAT vs C_MAT
#c) ABA_VER vs C_VER
#d) POL_VER vs C_VER
setwd("/media/nmauri/SHAULA/FCUL_MARGARIDAII_2022-10-03_AAATFM2HV")
library(edgeR)
library("openxlsx")
library(pheatmap)
#### 1. Counts on exonic regions matrix
m <- read.delim("counts_matrix.csv", sep = "\t", header = T)
dim(m) #[1] 208581     19
#Join count for each exon's gene
m1=aggregate(m[,-1], by=list(Category=m$gene_ID), FUN=sum)
rownames(m1)=m1$Category
m1=m1[,-1] 
dim(m1) #[1] 35197    18
#Eliminate sum of count =0
m2=m1[rowSums(m1)>0,]
dim(m2) #[1] 29405    18
colSums(m2)
#Eliminate means between replicates less than 1.
m3=m2[rowMeans(m2[,1:3])>1| rowMeans(m2[,4:6])>1|rowMeans(m2[,7:9])>1 |
      rowMeans(m2[,10:12])>1|rowMeans(m2[,13:15])>1|rowMeans(m2[,16:18])>1, ]
dim(m3) #[1] 26568    18
write.csv(m3, "DATASET_counts.csv", row.names = T, quote = F)
#### 2. Normalization by TMM method and CPMs. 
y <- DGEList(m3)
y <- calcNormFactors(y)
logcpm= cpm(y$counts, log = T)  
write.csv(logcpm, "DATASET_logCPMs.csv", row.names = T, quote = F)

par(mar=c(4,4,1,1))
plotMDS(y)
write.csv(y$samples, "TMM_norm_factor.csv", row.names = T, quote = F)

#### 3. Differential expression analysis based on counts.
a=y[,c("ABA_M1", "ABA_M2","ABA_M3","C_MAT1","C_MAT2","C_MAT3")]
b=y[,c("POL_M1", "POL_M2","POL_M3","C_MAT1","C_MAT2","C_MAT3")]
c=y[,c("ABA_V1", "ABA_V2","ABA_V3","C_VER1","C_VER2","C_VER3")]
d=y[,c("POL_V1", "POL_V2","POL_V3","C_VER1","C_VER2","C_VER3")]

g <- c(1,1,1,0,0,0)
model <- model.matrix(~g)
#change dataset
disp <- estimateDisp(d, model, robust=TRUE)
fit <- glmQLFit(disp, model, robust=TRUE)
test <- glmQLFTest(fit)
#class <- decideTestsDGE(test)
#number of degs by pvalue
#summary(class)
top <- topTags(test, n=Inf,sort.by = "none")
#Annotations file: PN40024.v4.1.REF.gff3
#annot=read.csv("Expressed_PN40024.v4.1.REF.b2g.tsv", sep = "\t", header = F)
#dim(annot) #26568    13
table= cbind(top$table[,-3], annot[,c(-1,-3,-4,-5,-6,-7,-8,-9,-13)])
colnames(table)= c("logFC","logCPM","PValue","FDR", "Annot", "G0_tags", "EC_reaction", "UniprotID")
nrow(table)
#write.xlsx(table, "ABA_MAT_vs_C_MAT_DGE.xlsx", rowNames=T)
#write.xlsx(table, "POL_MAT_vs_C_MAT_DGE.xlsx", rowNames=T)
#write.xlsx(table, "ABA_VER_vs_C_VER_DGE.xlsx", rowNames=T)
#write.xlsx(table, "POL_VER_vs_C_VER_DGE.xlsx", rowNames=T)

#Filter DEGs by FDR<=0.05 and abs(logFC)>=1
table_filt=table[table$FDR<=0.05 & abs(table$logFC)>=1,]
nrow(table_filt)
nrow(table_filt[table_filt$logFC<=-1,])
nrow(table_filt[table_filt$logFC>=1,])
#a
#[1] 1048
#[1] 294

case2control_top <- topTags(case2control, n=Inf, sort.by = "none")
case2control_H_top <- topTags(case2control_H, n=Inf, sort.by = "none")
case2control_PC_top <- topTags(case2control_PC, n=Inf, sort.by = "none")
control_PC2control_top <- topTags(control_PC2control, n=Inf, sort.by = "none")

write.csv(case2control_top[case2control_top$table$FDR<=0.05,], "case2control_fdr005.csv")
write.csv(case2control_H_top[case2control_H_top$table$FDR<=0.05,], "case2control_H_fdr005.csv")
write.csv(case2control_PC_top[case2control_PC_top$table$FDR<=0.05,], "case2control_PC_fdr005.csv")
write.csv(control_PC2control_top[control_PC2control_top$table$FDR<=0.05,], "control_PC2control_fdr005.csv")

write.csv(case2control_top[case2control_top$table$PValue<=0.05 & abs(case2control_top$table$logFC)>=1,], "case2control_pv005_fc1.csv")
write.csv(case2control_H_top[case2control_H_top$table$PValue<=0.05 & abs(case2control_H_top$table$logFC)>=1,], "case2control_H_pv005_fc1.csv")
write.csv(case2control_PC_top[case2control_PC_top$table$PValue<=0.05 & abs(case2control_PC_top$table$logFC)>=1,], "case2control_PC_pv005_fc1.csv")
write.csv(control_PC2control_top[control_PC2control_top$table$PValue<=0.05 & abs(control_PC2control_top$table$logFC)>=1,], "control_PC2control_pv005_fc1.csv")

#242 species in total
wc -l *pv005_fc1.csv
  169 case2control_H_pv005_fc1.csv
  167 case2control_PC_pv005_fc1.csv
   37 case2control_pv005_fc1.csv
   15 control_PC2control_pv005_fc1.csv
  388 total

cut -d',' -f1 *pv005_fc1.csv | sort -V | uniq | sed 's/"//g' > das.list
