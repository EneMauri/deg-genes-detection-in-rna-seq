#MAPPING
#hisat2-build PN40024.v4.REF.fasta PN40024.v4.REF
Paired_rnaseq_align(){
DIR="/media/nmauri/SHAULA/.../trimmed"
ABA_M2_11308AAD_TGCTCCGT-ATGGCTTC_R1_001_val_1.fq.gz
GENOME="PN40024.v4.REF"
#fastq-dump --gzip --split-files $1
#~/bin/TrimGalore-0.6.6/trim_galore --paired $1_1.fastq.gz $1_2.fastq.gz

#HISAT 1-STEP
hisat2 -k20 --dta -x $GENOME -1 ${DIR}/$1_R1_001_val_1.fq.gz -2 ${DIR}/$1_R2_001_val_2.fq.gz -S $1.hisat1.sam \
--novel-splicesite-outfile $1.hisat1.gtf
#HISAT 2-STEP
hisat2 -k1 --dta -x $GENOME -1 ${DIR}/$1_R1_001_val_1.fq.gz -2 ${DIR}/$1_R2_001_val_2.fq.gz -S $1.hisat2.sam \
--known-splicesite-infile $1.hisat1.gtf --novel-splicesite-outfile $1.hisat2.gtf

rm $1.hisat1.sam $1.hisat1.gtf
samtools view -Sbh $1.hisat2.sam > $1.hisat2.bam
samtools sort $1.hisat2.bam -o $1.sorted.bam
rm $1.hisat2.sam $1.hisat2.bam
samtools flagstat $1.sorted.bam > $1.sorted.flagstat
gatk MarkDuplicates I=$1.sorted.bam O=$1.markdup.bam \
M=$1.markdup.txt OPTICAL_DUPLICATE_PIXEL_DISTANCE= 2500 CREATE_INDEX=true
}

export -f Paired_rnaseq_align
parallel -j2 Paired_rnaseq_align {} < samples.list

